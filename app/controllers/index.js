function sendGet(){
	Alloy.Globals.TiLondonNodeExample.services_testGet(
		{name:$.tfName.value},
		function(error, args){
			if(error !== null){
				alert(error);
			} else {
				if(args !== null){
					$.lblSalutation.text = args.message;
				}
			}
	});
}

function sendPost(){
	Alloy.Globals.TiLondonNodeExample.services_testPost(
		{name:$.tfName.value},
		function(error, args){
			if(error !== null){
				alert(error);
			} else {
				if(args !== null){
					$.lblSalutation.text = args.message;
				}
			}
	});
}

$.index.open();
