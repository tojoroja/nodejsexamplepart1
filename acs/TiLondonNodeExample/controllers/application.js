function index(req, res) {
	res.render('index', { title: 'Welcome to Node.ACS!' });
}

function testGet(req, res){	
	var responseMessage = 'Hello ' + req.query["name"] + 'using GET request';
	res.send({'message': responseMessage});
}

function testPost(req, res){
	var responseMessage = 'Hello ' + req.body.name + 'using POST request';
	res.send({'message': responseMessage});
}
