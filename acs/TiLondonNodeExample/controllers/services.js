
function testGet(req, res) {
	var response = "Hello " + req.query.name + " from the testGet method";
	res.send({'message':response});
};

function testPost(req, res) {
	var response = "Hello " + req.body.name + " from the testPost method";
	res.send({'message':response});
};
