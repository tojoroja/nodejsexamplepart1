function Controller() {
    function sendGet() {
        Alloy.Globals.TiLondonNodeExample.services_testGet({
            name: $.tfName.value
        }, function(error, args) {
            null !== error ? alert(error) : null !== args && ($.lblSalutation.text = args.message);
        });
    }
    function sendPost() {
        Alloy.Globals.TiLondonNodeExample.services_testPost({
            name: $.tfName.value
        }, function(error, args) {
            null !== error ? alert(error) : null !== args && ($.lblSalutation.text = args.message);
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.index = Ti.UI.createWindow({
        backgroundColor: "white",
        layout: "vertical",
        id: "index"
    });
    $.__views.index && $.addTopLevelView($.__views.index);
    $.__views.lblSalutation = Ti.UI.createLabel({
        id: "lblSalutation",
        top: "50",
        height: "50",
        width: "200"
    });
    $.__views.index.add($.__views.lblSalutation);
    $.__views.btnGet = Ti.UI.createButton({
        id: "btnGet",
        title: "GET",
        top: "30",
        width: "120",
        height: "45"
    });
    $.__views.index.add($.__views.btnGet);
    sendGet ? $.__views.btnGet.addEventListener("click", sendGet) : __defers["$.__views.btnGet!click!sendGet"] = true;
    $.__views.btnPost = Ti.UI.createButton({
        id: "btnPost",
        title: "POST",
        top: "30",
        width: "120",
        height: "45"
    });
    $.__views.index.add($.__views.btnPost);
    sendPost ? $.__views.btnPost.addEventListener("click", sendPost) : __defers["$.__views.btnPost!click!sendPost"] = true;
    $.__views.tfName = Ti.UI.createTextField({
        id: "tfName",
        hintText: "name",
        top: "30"
    });
    $.__views.index.add($.__views.tfName);
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.index.open();
    __defers["$.__views.btnGet!click!sendGet"] && $.__views.btnGet.addEventListener("click", sendGet);
    __defers["$.__views.btnPost!click!sendPost"] && $.__views.btnPost.addEventListener("click", sendPost);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;