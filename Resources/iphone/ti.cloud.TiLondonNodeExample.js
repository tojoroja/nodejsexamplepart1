function serialize(obj) {
    if (null !== obj && "object" == typeof obj) {
        var str = [];
        for (var p in obj) obj.hasOwnProperty(p) && str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return "?" + str.join("&");
    }
    return "";
}

function InvokeService(path, method, data, cb) {
    Ti.API.warn(path);
    if ("function" == typeof data) {
        cb = data;
        data = null;
    }
    if ("function" != typeof cb) throw new Error("callback must be provided!");
    var xhr = Ti.Network.createHTTPClient();
    xhr.onerror = function(e) {
        cb(e.error);
    };
    xhr.onload = function() {
        var r = this.responseText;
        try {
            -1 != xhr.getResponseHeader("content-type").indexOf("json") && (r = JSON.parse(r));
        } catch (E) {}
        cb(null, r);
    };
    "/" == exports.URL.match("/$") && 0 == path.indexOf("/") ? Ti.API.warn(exports.URL + path.substring(1)) : Ti.API.warn(exports.URL + path);
    "/" == exports.URL.match("/$") && 0 == path.indexOf("/") ? xhr.open(method, exports.URL + path.substring(1)) : xhr.open(method, exports.URL + path);
    xhr.send(data);
}

var url = Ti.App.Properties.getString("acs-service-baseurl-TiLondonNodeExample");

if (!url) throw new Error("Url not found by acs-service-baseurl-TiLondonNodeExample.");

exports.URL = url.replace(/^\s+|\s+$/g, "") ? url.replace(/^\s+|\s+$/g, "") : "http://localhost:8080";

exports.application_index = function(data, cb) {
    var path = [];
    path.push("/");
    InvokeService(path.join(""), "GET", data, cb);
};

exports.services_testGet = function(data, cb) {
    var path = [];
    path.push("/testGet");
    path.push(serialize(data));
    InvokeService(path.join(""), "GET", cb);
};

exports.services_testPost = function(data, cb) {
    var path = [];
    path.push("/testPost");
    InvokeService(path.join(""), "POST", data, cb);
};